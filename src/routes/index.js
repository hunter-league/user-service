const getUsers = require('./get-users')
const getUser = require('./get-user')
const deleteUser = require('./delete-user')
const updateUser = require('./update-user')
const createUser = require('./create-user')

module.exports = {
    getUsers,
    getUser,
    deleteUser,
    updateUser,
    createUser
}
