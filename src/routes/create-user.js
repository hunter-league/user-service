User = require('../models/user');

/**
 * Create a user.
 * @name create-user
 * @param {Object} body - The request body
 * @param {string} body.firstName - User first name
 * @param {string} body.lastName - User's last name
 * @param {string} body.emailAddress - Users's email address
 * @example
 * curl -X POST -H "Content-Type: application/json" -d '{"firstName":"Jalissa","lastName":"Doe","emailAddress": "jdoe@gmail.com"}' https://host/user/new/
 *
 * {
 *   id: '000000000-0000-0000-0000-0000000000000',
 *   firstName: 'Jalissa',
 *   lastName: 'Doe',
 *   emailAddress: 'jdoe@gmail.com',
 * }
 */
module.exports = (req, res, next) => {
    const user = new User();
    user.firstName = req.body.firstName;
    user.lastName = req.body.lastName;
    user.userName = req.body.userName;
    user.emailAddress = req.body.emailAddress;
    user.password = req.body.password;
    user.phoneNumber = req.body.phoneNumber;
    user.gender = req.body.gender;


// save the user and check for errors
    user.save(function (err) {
        // Check for validation error
        if (err) {
            res.json(err);
        }
        else {
            res.json({
                message: 'New user created!',
                data: user
            });
        }
    });
}
