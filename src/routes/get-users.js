User = require('../models/user');

/**
 * Get a list of users.
 * @name get-users
 * @param {Object} [query] - The request URL query parameters
 * @example
 * curl https://host/users
 *
 * [
 *   {
 *     id: '00000000-0000-0000-0000-000000000000',
 *     firstName: 'Jalissa',
 *     lastName: 'Doe',
 *     emailAddress: 'jdoe@gmail.com'
 *     createdAt: '2017-10-18T00:00:00.000Z',
 *     updatedAt: '2017-10-18T00:00:00.000Z'
 *   }
 * ]
 */
module.exports = (req, res, next) => {
    User.get(function (err, users) {
        if (err) {
            res.json({
                status: res.statusCode,
                message: err,
            });
        }
        res.json({
            status: res.statusCode,
            message: "Users retrieved successfully",
            data: users
        });
    });
}
