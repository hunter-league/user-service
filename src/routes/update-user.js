User = require('../models/user');

module.exports = (req, res, next) => {
    User.findById(req.params._id, function (err, user) {
        if (err) {
            res.send(err);
        }
        user.firstName = req.body.firstName;
        user.lastName = req.body.lastName;
        user.userName = req.body.userName;
        user.emailAddress = req.body.emailAddress;
        user.password = req.body.password;
        user.phoneNumber = req.body.phoneNumber;
        user.gender = req.body.gender;

// save the user and check for errors
        user.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'User information updated',
                data: user
            });
        });
    });
}
