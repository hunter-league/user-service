User = require('../models/user');

module.exports = (req, res, next) => {
    User.deleteOne({
        id: req.params._id
    }, function (err, user) {
        if (err) {
            res.send(err);
        }
        res.json({
            status: "success",
            message: 'User deleted'
        });
    });
}
