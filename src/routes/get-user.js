User = require('../models/user');
/**
 * Get a user.
 * @name get-user
 * @param {Object} [query] - The request URL query parameters
 * @param {string} [query.name] - Name of project to filter by (optional)
 * @param {true|false} [query.is_archived] - default is false - set to true to return archived projects
 * @example
 * curl https://host/user/:id
 *
 * [
 *   {
 *     id: '00000000-0000-0000-0000-000000000000',
 *     name: 'My Project',
 *     metadata: {},
 *     createdAt: '2017-10-18T00:00:00.000Z',
 *     updatedAt: '2017-10-18T00:00:00.000Z'
 *   }
 * ]
 */
module.exports = (req, res, next) => {
    console.log('Hitting endpoint...')
    User.findById(req.params._id, function (err, user) {
        if (err) {
            res.send(err);
        }
        console.log('PARAMS', req.params)
        res.json({
            message: 'Loading User details...',
            data: user
        });
    });
}
