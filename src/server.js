// Import express
let express = require('express');
let bodyParser = require('body-parser');
let mongoose = require('mongoose');
let server = express();
let routes = require("./routes/index");
const port = process.env.PORT || 9999;

server.use(bodyParser.urlencoded({
    extended: true
}));
server.use(bodyParser.json());

// Connect to Mongoose and set connection variable
mongoose.connect('mongodb://localhost/theleague', { useNewUrlParser: true});

var db = mongoose.connection;

// Added check for DB connection
if(!db) {
    console.log("Error connecting db")
} else {
    console.log("Db connected successfully")

}


// Send message for default URL
server.get('/', (req, res) => res.send('Welcome to \"The League\" User Service!'));

// Use Api routes in the App
server.get('/user/:_id', routes.getUser)
server.get('/users', routes.getUsers)
server.post('/user/new', routes.createUser)
server.put('/user/update/:_id', routes.updateUser)
server.delete('/user/delete/:_id', routes.deleteUser)



// Launch app to listen to specified port
server.listen(port, function () {
    console.log("Running \"The League\" User Service on port " + port);
});
