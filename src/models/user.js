// user.js
const mongoose = require('mongoose');
var uuid = require('node-uuid');

// Setup schema
const userSchema = mongoose.Schema({
    _id: {
        type: String,
        default: uuid.v4
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    userName: {
      type: String,
      required: true
    },
    emailAddress: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    gender: String,
    phoneNumber: String,
    create_date: {
        type: Date,
        default: Date.now
    }
});
// Export Contact model
const User = module.exports = mongoose.model('user', userSchema);
module.exports.get = function (callback, limit) {
    User.find(callback).limit(limit);
}
