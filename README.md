# The League

U# Prerequisites
 * [Node JS (v8.0.0)](https://nodejs.org/en/download/)
 * [npm (v6.4.1)](https://www.npmjs.com/get-npm)
 * [nvm](https://github.com/creationix/nvm#installation)
 * [Docker Community Edition](https://www.docker.com/get-started)
 
 # Overview
 
 ![](sources/theleague.png?raw=true)
 
 The "User Service" is one of the three services used to support "The League" application. Its goal is to create, update and read user accounts for the application.le.
 
 # Getting Started
### Docker
 * `nvm use 10`
 * `npm install`
 * `npm run dev`
 
 ## Local
 * `nvm use 10`
 * `npm install`
 * `npm start`
 
## Commands
|  Script  |  Description | Command  |
|---|---|---|
| start  | Runs service with Node.js  | `npm run start`  |
|dev   |  Runs service with Docker | `npm run dev`  |
|test   | Runs all test  | `npm run test`  |

## Endpoints


