FROM node:10-slim
ENV DIR=/user-service/
ENV SCRIPTS=./scripts
WORKDIR $DIR

COPY package*.json $DIR

RUN  apt-get update -qq && npm install

COPY . $DIR

EXPOSE 8889

CMD $SCRIPTS/docker-entrypoint.sh
